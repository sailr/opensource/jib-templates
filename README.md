# jib-templates

Template Manifests for Kubernetes

This project is used solely for allowing Kubernetes Manifests to continue to evolve without required changes to [jib](https://gitlab.com/sailr/opensource/jib). This design decision is directly correlated to the evolution and pace of [Kubernetes development](https://github.com/kubernetes/kubernetes/releases)

## Getting Started

Clone this repo for a base set of configurable Kubernetes manifests

## Documentation

Manifest [documentation](manifests/README.md) is available in the manifests directory. 
