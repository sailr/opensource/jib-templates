# jib-templates

Template Manifests for Kubernetes

## Overview

**Configurable Variables:**

* `_NEW_APP_NAME` - The name of the application
* `_NEW_APP_REGISTRY` - The registry URI (i.e. registry.gitlab.com)
* `_NEW_APP_REGISTRY_SECRET` - jib requires that private registries have a [cluster secret](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-by-providing-credentials-on-the-command-line)
* `_NEW_APP_REPLICAS` - The number of container replicas running at any one point in time. Default: 1
* `_NEW_APP_REVISION_HISTORY` - The number of previous container versions left in the cluster for rollback. Default: 1
* `_NEW_APP_SERVICE_TYPE` - The exposure level of the application (LoadBalancer: exposed to the internet, NodePort: exposed as the same port on every node, ClusterIP: exposed only within the cluster, ExternalName: maps to the value of `externalName` field returning a CNAME value only - no proxy). Default: 1
* `_NEW_APP_EXPOSED_PORT` - The port you want your application exposed on within the cluster (default: 80)
* `_NEW_APP_RUNNING_PORT` - The application's exposed port (this is the port you use to access it locally - i.e. localhost:**3000**)
